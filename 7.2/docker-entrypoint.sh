#!/bin/bash
set -e

if [ "$PHP_XDEBUG_ENABLED" = true ]; then
  # sed -i '/zend_extension/s/;//' /etc/php5/conf.d/xdebug.ini
  cat << EOF > ${PHP_INI_DIR}/conf.d/xdebug.ini
zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)
xdebug.remote_enable=1
xdebug.remote_host=$PHP_XDEBUG_HOST
xdebug.remote_port=$PHP_XDEBUG_PORT
EOF
fi

# If first arg starts with hyphen, assume user is trying to run
# apache2-foreground directly with some arguments.
if [ "${1:0:1}" = '-' ]; then
  set -- apache2-foreground "$@"
fi

exec "$@"
