#!/bin/sh
set -e

if [ "$PHP_XDEBUG_ENABLED" = true ]; then
  # sed -i '/zend_extension/s/;//' /etc/php5/conf.d/xdebug.ini
  cat << EOF > /etc/php5/conf.d/xdebug.ini
zend_extension=xdebug.so
xdebug.remote_enable=1
xdebug.remote_host=$PHP_XDEBUG_HOST
xdebug.remote_port=$PHP_XDEBUG_PORT
EOF
fi

# If the user is trying to run httpd directly with some arguments, then
# pass them to httpd.
if [ "${1:0:1}" = '-' ]; then
  set -- httpd "$@"
fi

# Make sure foreground is set and PID files are gone
if [ "$1" == "httpd" ]; then
  set -- "$@" -DFOREGROUND
  rm -f /run/apache2/httpd.pid
fi

exec "$@"
